#sir-messenger

Sir[1] messenger is a socket server written on Node.js for online turn-based games. It is the middle layer between clients and other microservices in the sir platform.


[1] The name Sir is an tribute to a dear Venezuelan cuatrist called [Sir Augusto Ramirez](http://es.wikipedia.org/wiki/Sir_Augusto_Ram%C3%ADrez)
